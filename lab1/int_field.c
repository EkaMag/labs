//
// Created by Eka on 27.05.2024.
//

#include "int_field.h"

void buffer_int_arrays_ptr_push(int* new_array) {
    buffer_int_arrays_ptr = realloc(buffer_int_arrays_ptr, (buffer_int_arrays_ptr_size + 1) * sizeof(int*));
    buffer_int_arrays_ptr[buffer_int_arrays_ptr_size] = new_array;
    buffer_int_arrays_ptr_size++;
}

void print_int_vector(Vector* vec) {
    for (size_t i = 0; i < vec->size; i++) {
        printf("%d ", *((int*)at(vec, i)->data));
    }
    printf("\n");
}

void int_vector_sum(Vector* vector_sum, Vector* vec1, Vector* vec2) {
    if (vec1->size != vec2->size) {
        fprintf(stderr, "Vector sizes do not match\n");
        exit(EXIT_FAILURE);
    }

    int* array = calloc(vec1->size, sizeof(int));
    buffer_int_arrays_ptr_push(array);

    for (size_t i = 0; i < vec2->size; i++) {
        Element* elem1 = at(vec2, i);
        Element* elem2 = at(vec1, i);

        array[i] = *((int*)elem1->data) + *((int*)elem2->data);

        Element finalElem = {elem1->size, &array[i]};
        push_back(vector_sum, finalElem);
    }
}

int int_vector_scalar_product(Vector* vec1, Vector* vec2) {
    if (vec1->size != vec2->size) {
        fprintf(stderr, "Vector sizes do not match\n");
        exit(EXIT_FAILURE);
    }

    int scalar = 0;
    for (size_t i = 0; i < vec2->size; i++) {
        int elem1 = *(int*)at(vec2, i)->data;
        int elem2 = *(int*)at(vec1, i)->data;
        scalar += elem1 * elem2;
    }
    return scalar;
}