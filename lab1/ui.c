//
// Created by Eka on 30.05.2024.
//

#include "ui.h"

// Максимальное количество векторов, которое можно создать
#define MAX_VECTORS 10

typedef enum {
    INT, DOUBLE
} ElementType;

void print_menu() {
    printf("\n--- Vector Operations Menu ---\n");
    printf("1. Create new vector\n");
    printf("2. Add element to vector\n");
    printf("3. Insert element into vector\n");
    printf("4. Replace element in vector\n");
    printf("5. Sum two vectors\n");
    printf("6. Scalar product of two vectors\n");
    printf("7. Print vector\n");
    printf("8. Exit\n");
    printf("Choose an option: ");
}

void create_vector(Vector *vectors, int *vector_count, ElementType *types) {
    if (*vector_count >= MAX_VECTORS) {
        printf("Maximum number of vectors reached.\n");
        return;
    }
    init(&vectors[*vector_count]);
    printf("Choose element type (0 for int, 1 for double): ");
    int type_choice;
    if (scanf("%d", &type_choice) != 1 || (type_choice != 0 && type_choice != 1)) {
        printf("Invalid type choice. Please enter 0 for int or 1 for double.\n");
        return;
    }
    types[*vector_count] = (type_choice == 0) ? INT : DOUBLE;
    printf("Vector %d created.\n", *vector_count);
    (*vector_count)++;
}

void add_element_to_vector(Vector *vectors, int vector_count, ElementType *types) {
    int vec_id;
    char c;
    printf("Enter vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec_id < 0 || vec_id >= vector_count) {
        printf("Invalid vector ID.\n");
        return;
    }

    if (types[vec_id] == INT) {
        int *a = calloc(1, sizeof(int));
        buffer_int_arrays_ptr_push(a);

        printf("Enter integer value to add: ");
        if (scanf("%d", &a[0]) != 1) {
            printf("Invalid integer value.\n");
            free(a);
            return;
        }
        push_back(&vectors[vec_id], (Element) {sizeof(int), &a[0]});
    } else {
        double *a = calloc(1, sizeof(double));
        buffer_double_arrays_ptr_push(a);

        printf("Enter double value to add: ");
        if (scanf("%lf", &a[0]) != 1) {
            printf("Invalid double value.\n");
            free(a);
            return;
        }
        push_back(&vectors[vec_id], (Element) {sizeof(double), &a[0]});
    }

    printf("Value added to vector %d.\n", vec_id);
}

void insert_element_into_vector(Vector *vectors, int vector_count, ElementType *types) {
    int vec_id, index;
    char c;

    printf("Enter vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec_id < 0 || vec_id >= vector_count) {
        printf("Invalid vector ID.\n");
        return;
    }
    printf("Enter index to insert at: ");
    if (scanf("%d", &index) != 1) {
        printf("Invalid index.\n");
        return;
    }

    if (types[vec_id] == INT) {
        int *a = calloc(1, sizeof(int));
        buffer_int_arrays_ptr_push(a);

        printf("Enter integer value to insert: ");
        if (scanf("%d", &a[0]) != 1) {
            printf("Invalid integer value.\n");
            free(a);
            return;
        }
        insert(&vectors[vec_id], index, (Element) {sizeof(int), &a[0]});
    } else {
        double *a = calloc(1, sizeof(double));
        buffer_double_arrays_ptr_push(a);

        printf("Enter double value to insert: ");
        if (scanf("%lf", &a[0]) != 1) {
            printf("Invalid double value.\n");
            free(a);
            return;
        }
        insert(&vectors[vec_id], index, (Element) {sizeof(double), &a[0]});
    }

    printf("Value inserted into vector %d at index %d.\n", vec_id, index);
}

void replace_element_in_vector(Vector *vectors, int vector_count, ElementType *types) {
    int vec_id, index;
    char c;

    printf("Enter vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec_id < 0 || vec_id >= vector_count) {
        printf("Invalid vector ID.\n");
        return;
    }
    printf("Enter index to replace: ");
    if (scanf("%d", &index) != 1) {
        printf("Invalid index.\n");
        return;
    }

    if (types[vec_id] == INT) {
        int *a = calloc(1, sizeof(int));
        buffer_int_arrays_ptr_push(a);

        printf("Enter new integer value: ");
        if (scanf("%d", &a[0]) != 1) {
            printf("Invalid integer value.\n");
            free(a);
            return;
        }
        replace(&vectors[vec_id], index, (Element) {sizeof(int), &a[0]});
    } else {
        double *a = calloc(1, sizeof(double));
        buffer_double_arrays_ptr_push(a);

        printf("Enter new double value: ");
        if (scanf("%lf", &a[0]) != 1) {
            printf("Invalid double value.\n");
            free(a);
            return;
        }
        replace(&vectors[vec_id], index, (Element) {sizeof(double), &a[0]});
    }

    printf("Value at index %d in vector %d replaced.\n", index, vec_id);
}

void sum_vectors(Vector *vectors, int vector_count, ElementType *types) {
    int vec1_id, vec2_id;
    char c;
    printf("Enter first vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec1_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    printf("Enter second vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec2_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec1_id < 0 || vec1_id >= vector_count || vec2_id < 0 || vec2_id >= vector_count) {
        printf("Invalid vector IDs.\n");
        return;
    }

    if (types[vec1_id] != types[vec2_id]) {
        printf("Vector types do not match.\n");
        return;
    }

    Vector result;
    init(&result);

    if (types[vec1_id] == INT) {
        int_vector_sum(&result, &vectors[vec1_id], &vectors[vec2_id]);
    } else {
        double_vector_sum(&result, &vectors[vec1_id], &vectors[vec2_id]);
    }

    printf("Sum of vector %d and vector %d:\n", vec1_id, vec2_id);
    if (types[vec1_id] == INT) {
        print_int_vector(&result);
    } else {
        print_double_vector(&result);
    }
    free_vector(&result);
}

void scalar_product_of_vectors(Vector *vectors, int vector_count, ElementType *types) {
    int vec1_id, vec2_id;
    char c;
    printf("Enter first vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec1_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    printf("Enter second vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec2_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec1_id < 0 || vec1_id >= vector_count || vec2_id < 0 || vec2_id >= vector_count) {
        printf("Invalid vector IDs.\n");
        return;
    }

    if (types[vec1_id] != types[vec2_id]) {
        printf("Vector types do not match.\n");
        return;
    }

    if (types[vec1_id] == INT) {
        int result = int_vector_scalar_product(&vectors[vec1_id], &vectors[vec2_id]);
        printf("Scalar product of vector %d and vector %d: %d\n", vec1_id, vec2_id, result);
    } else {
        double result = double_vector_scalar_product(&vectors[vec1_id], &vectors[vec2_id]);
        printf("Scalar product of vector %d and vector %d: %lf\n", vec1_id, vec2_id, result);
    }
}

void print_vector_content(Vector *vectors, int vector_count, ElementType *types) {
    int vec_id;
    char c;
    printf("Enter vector ID (0 to %d): ", vector_count - 1);
    if (scanf("%d%c", &vec_id, &c) != 2 || c != '\n') {
        printf("Invalid vector ID.\n");
        return;
    }
    if (vec_id < 0 || vec_id >= vector_count) {
        printf("Invalid vector ID.\n");
        return;
    }
    if (types[vec_id] == INT) {
        print_int_vector(&vectors[vec_id]);
    } else {
        print_double_vector(&vectors[vec_id]);
    }
}

void user_interface() {
    Vector vectors[MAX_VECTORS];
    ElementType types[MAX_VECTORS];
    int vector_count = 0;
    int choice;

    while (1) {
        print_menu();
        if (scanf("%d", &choice) != 1) {
            printf("Invalid choice. Please try again.\n");
            while (getchar() != '\n'); // clear invalid input
            continue;
        }
        switch (choice) {
            case 1:
                create_vector(vectors, &vector_count, types);
                break;
            case 2:
                add_element_to_vector(vectors, vector_count, types);
                break;
            case 3:
                insert_element_into_vector(vectors, vector_count, types);
                break;
            case 4:
                replace_element_in_vector(vectors, vector_count, types);
                break;
            case 5:
                sum_vectors(vectors, vector_count, types);
                break;
            case 6:
                scalar_product_of_vectors(vectors, vector_count, types);
                break;
            case 7:
                print_vector_content(vectors, vector_count, types);
                break;
            case 8:
                for (int i = 0; i < vector_count; i++) {
                    free_vector(&vectors[i]);
                }
                for (int i = 0; i < buffer_double_arrays_ptr_size; ++i) {
                    free(buffer_double_arrays_ptr[i]);
                }
                for (int i = 0; i < buffer_int_arrays_ptr_size; ++i) {
                    free(buffer_int_arrays_ptr[i]);
                }
                printf("Exiting...\n");
                return;
            default:
                printf("Invalid choice. Please try again.\n");
                break;
        }
    }
}
