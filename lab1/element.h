//
// Created by Eka on 26.05.2024.
//

#ifndef LABS_ELEMENT_H
#define LABS_ELEMENT_H

#include <stdlib.h>

// Структура элемента вектора
typedef struct {
    size_t size;  // Размер данных в битах
    void* data;   // Указатель на данные
} Element;

#endif //LABS_ELEMENT_H
