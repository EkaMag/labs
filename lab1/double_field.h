//
// Created by Eka on 30.05.2024.
//

#ifndef LABS_DOUBLE_FIELD_H
#define LABS_DOUBLE_FIELD_H

#include "vector.h"

static double** buffer_double_arrays_ptr = NULL;
static int buffer_double_arrays_ptr_size = 0;

void buffer_double_arrays_ptr_push(double* new_array);
void double_vector_sum(Vector* vector_sum, Vector* vec1, Vector* vec2);
double double_vector_scalar_product(Vector* vec1, Vector* vec2);
void print_double_vector(Vector* vec);

#endif //LABS_DOUBLE_FIELD_H
