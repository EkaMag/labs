# Vector Operations Program

This program provides a console-based user interface for performing various operations on vectors, including creating vectors, adding elements, inserting elements, replacing elements, summing vectors, and calculating the scalar product of vectors. The program supports vectors with `int` and `double` elements.

## Prerequisites

Make sure you have `gcc` and `make` installed on your system.

## Build Instructions

To compile the main program and the test program, run:

```sh
make
```

This will create the executables in the `./out` directory.

## Running the Main Program

To run the main program, execute:

```sh
./out/main
```

### Using the User Interface

When you run the main program, you will see the following menu:

```
--- Vector Operations Menu ---
1. Create new vector
2. Add element to vector
3. Insert element into vector
4. Replace element in vector
5. Sum two vectors
6. Scalar product of two vectors
7. Print vector
8. Exit
Choose an option:
```

Follow the on-screen instructions to perform operations:

1. **Create new vector**: Creates a new vector. You will be prompted to choose the element type (`0` for `int`, `1` for `double`).
2. **Add element to vector**: Adds an element to a specified vector. You will be prompted to enter the vector ID and the element value.
3. **Insert element into vector**: Inserts an element at a specified index in a vector. You will be prompted to enter the vector ID, the index, and the element value.
4. **Replace element in vector**: Replaces an element at a specified index in a vector. You will be prompted to enter the vector ID, the index, and the new element value.
5. **Sum two vectors**: Sums two vectors and prints the result. You will be prompted to enter the IDs of the two vectors.
6. **Scalar product of two vectors**: Calculates and prints the scalar product of two vectors. You will be prompted to enter the IDs of the two vectors.
7. **Print vector**: Prints the contents of a specified vector. You will be prompted to enter the vector ID.
8. **Exit**: Exits the program and cleans up all allocated resources.

## Running Tests

To run the test program, execute:

```sh
make test
```

This will compile and run the test executable.

### Clean Up

To clean up all generated files, run:

```sh
make clean
```

This will remove the `./out` directory and all compiled object files and executables.

## Files

- **main.c**: Contains the main program and the user interface.
- **vector.c**, **vector.h**: Implements the vector data structure and operations.
- **element.c**, **element.h**: Implements the element structure.
- **int_field.c**, **int_field.h**: Implements operations specific to `int` elements.
- **double_field.c**, **double_field.h**: Implements operations specific to `double` elements.
- **test_main.c**: Contains test cases for the vector operations.
- **Makefile**: Contains build instructions for the main program and the test program.
