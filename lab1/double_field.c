//
// Created by Eka on 30.05.2024.
//

#include "double_field.h"

void buffer_double_arrays_ptr_push(double* new_array) {
    buffer_double_arrays_ptr = realloc(buffer_double_arrays_ptr, (buffer_double_arrays_ptr_size + 1) * sizeof(double*));
    buffer_double_arrays_ptr[buffer_double_arrays_ptr_size] = new_array;
    buffer_double_arrays_ptr_size++;
}

void print_double_vector(Vector* vec) {
    for (size_t i = 0; i < vec->size; i++) {
        printf("%lf ", *((double *)at(vec, i)->data));
    }
    printf("\n");
}

void double_vector_sum(Vector* vector_sum, Vector* vec1, Vector* vec2) {
    if (vec1->size != vec2->size) {
        fprintf(stderr, "Vector sizes do not match\n");
        exit(EXIT_FAILURE);
    }

    double* array = calloc(vec1->size, sizeof(double));
    buffer_double_arrays_ptr_push(array);

    for (size_t i = 0; i < vec2->size; i++) {
        Element* elem1 = at(vec2, i);
        Element* elem2 = at(vec1, i);

        array[i] = *((double*)elem1->data) + *((double*)elem2->data);

        Element finalElem = {elem1->size, &array[i]};
        push_back(vector_sum, finalElem);
    }
}

double double_vector_scalar_product(Vector* vec1, Vector* vec2) {
    if (vec1->size != vec2->size) {
        fprintf(stderr, "Vector sizes do not match\n");
        exit(EXIT_FAILURE);
    }

    double scalar = 0;
    for (size_t i = 0; i < vec2->size; i++) {
        double elem1 = *(double*)at(vec2, i)->data;
        double elem2 = *(double*)at(vec1, i)->data;
        scalar += elem1 * elem2;
    }
    return scalar;
}