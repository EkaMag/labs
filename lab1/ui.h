//
// Created by Eka on 30.05.2024.
//

#ifndef LABS_UI_H
#define LABS_UI_H

#include "stdio.h"
#include "stdlib.h"
#include "vector.h"
#include "int_field.h"
#include "double_field.h"

void user_interface();

#endif //LABS_UI_H
