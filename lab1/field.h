//
// Created by Eka on 27.05.2024.
//

#ifndef LABS_FIELD_H
#define LABS_FIELD_H

#include "vector.h"

typedef struct {
    Vector* (*vector_sum)(Vector* vec1, Vector* vec2); // Сложение векторов
    Element (*scalar_vector_product)(Vector* vec1, Vector* vec2); // Скалярное произведение векторов
} Field;

#endif //LABS_FIELD_H
