//
// Created by Eka on 26.05.2024.
//

#ifndef LABS_VECTOR_H
#define LABS_VECTOR_H

#include <stdlib.h>
#include <stdio.h>
#include "element.h"

// Структура вектора
typedef struct {
    Element* array; // Указатель на массив элементов
    size_t size;    // Размер вектора
    size_t capacity; // Емкость вектора
} Vector;

void init(Vector* vec);
void push_back(Vector* vec, Element data);
void insert(Vector* vec, size_t index, Element value);
void replace(Vector* vec, size_t index, Element value);
void free_vector(Vector* vec);
void print_vector(Vector* vec);
Element* at(Vector* vec, size_t index);

#endif //LABS_VECTOR_H
