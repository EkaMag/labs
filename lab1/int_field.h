//
// Created by Eka on 30.05.2024.
//

#ifndef LABS_INT_FIELD_H
#define LABS_INT_FIELD_H

#include "vector.h"

static int** buffer_int_arrays_ptr = NULL;
static int buffer_int_arrays_ptr_size = 0;

void buffer_int_arrays_ptr_push(int* new_array);
void int_vector_sum(Vector* vector_sum, Vector* vec1, Vector* vec2);
int int_vector_scalar_product(Vector* vec1, Vector* vec2);
void print_int_vector(Vector* vec);

#endif //LABS_INT_FIELD_H
