//
// Created by Eka on 26.05.2024.
//

#include "vector.h"

// Инициализация вектора
void init(Vector* vec) {
    vec->array = NULL;
    vec->size = 0;
    vec->capacity = 0;
}

// Добавление элемента в конец вектора
void push_back(Vector* vec, Element data) {
    // Если емкости недостаточно, увеличиваем размер массива
    if (vec->size >= vec->capacity) {
        vec->capacity = (vec->capacity == 0) ? 16 : vec->capacity * 2;
        vec->array = realloc(vec->array, vec->capacity * sizeof(Element));
        if (!vec->array) {
            fprintf(stderr, "push_back: Memory allocation failed\n");
            exit(EXIT_FAILURE);
        }
    }
    // Копируем данные в элемент вектора
    vec->array[vec->size] = data;
    vec->size++;
}

// Вставка элемента по индексу
void insert(Vector* vec, size_t index, Element value) {
    if (index > vec->size) {
        fprintf(stderr, "insert: Index out of range\n");
        exit(EXIT_FAILURE);
    }

    if (vec->size >= vec->capacity) {
        vec->capacity = (vec->capacity == 0) ? 1 : vec->capacity * 2;
        vec->array = realloc(vec->array, vec->capacity * sizeof(double));
        if (!vec->array) {
            fprintf(stderr, "insert: Memory allocation failed\n");
            exit(EXIT_FAILURE);
        }
    }

    for (size_t i = vec->size; i > index; i--) {
        vec->array[i] = vec->array[i - 1];
    }
    vec->array[index] = value;
    vec->size++;
}

void replace(Vector* vec, size_t index, Element value) {
    if (index >= vec->size) {
        fprintf(stderr, "replace: Index out of range\n");
        exit(EXIT_FAILURE);
    }
    if (value.size != vec->array[index].size) {
        fprintf(stderr, "replace:Incorrect value type\n");
        exit(EXIT_FAILURE);
    }
    vec->array[index] = value;
}



// Получение элемента по индексу
Element* at(Vector* vec, size_t index) {
    if (index >= vec->size) {
        fprintf(stderr, "at: Index out of range\n");
        exit(EXIT_FAILURE);
    }
    return &(vec->array[index]);
}

void free_vector(Vector* vec) {
    free(vec->array); // Освобождаем память, занятую массивом элементов
    init(vec); // Обнуляем вектор
}