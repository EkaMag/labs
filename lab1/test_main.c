#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
#include "element.h"
#include "int_field.h"

void test_push_back() {
    Vector vec;
    init(&vec);

    int value1 = 1, value2 = 2;
    push_back(&vec, (Element){ sizeof(int), &value1 });
    push_back(&vec, (Element){sizeof(int), &value2 });

    if (*(int*)vec.array[0].data == 1 && *(int*)vec.array[1].data == 2) {
        printf("test_push_back passed\n");
    } else {
        printf("test_push_back failed\n");
    }

    free_vector(&vec);
}

void test_insert() {
    Vector vec;
    init(&vec);

    int value1 = 1, value2 = 2, value3 = 3;
    push_back(&vec, (Element){sizeof(int), &value1});
    push_back(&vec, (Element){sizeof(int), &value2});
    insert(&vec, 1, (Element){sizeof(int), &value3});

    if (*(int*)vec.array[1].data == 3) {
        printf("test_insert passed\n");
    } else {
        printf("test_insert failed\n");
    }

    free_vector(&vec);
}

void test_replace() {
    Vector vec;
    init(&vec);

    int value1 = 1, value2 = 2, value3 = 3;
    push_back(&vec, (Element){sizeof(int), &value1});
    push_back(&vec, (Element){sizeof(int), &value2});
    replace(&vec, 1, (Element){sizeof(int), &value3,});


    if (*(int*)vec.array[1].data == 3) {
        printf("test_replace passed\n");
    } else {
        printf("test_replace failed\n");
    }

    free_vector(&vec);
}

void test_operations() {
    Vector vec1, vec2, result;
    init(&vec1);
    init(&vec2);
    init(&result);

    int value1 = 1, value2 = 2, value3 = 3;
    int value4 = 4, value5 = 5, value6 = 6;

    push_back(&vec1, (Element){sizeof(int), &value1});
    push_back(&vec1, (Element){sizeof(int), &value2});
    push_back(&vec1, (Element){sizeof(int), &value3});

    push_back(&vec2, (Element){sizeof(int), &value4});
    push_back(&vec2, (Element){sizeof(int), &value5});
    push_back(&vec2, (Element){sizeof(int), &value6});

    // Тестирование суммы векторов
    int_vector_sum(&result, &vec1, &vec2);
    if (*(int*)result.array[0].data == 5 &&
        *(int*)result.array[1].data == 7 &&
        *(int*)result.array[2].data == 9) {
        printf("test_add passed\n");
    } else {
        printf("test_add failed\n");
    }
    free_vector(&result);

    // Тестирование скалярного произведения
    int scalar = int_vector_scalar_product(&vec1, &vec2);
    if (scalar == 32) {
        printf("test_multiply_scalar passed\n");
    } else {
        printf("test_multiply_scalar failed\n");
    }

    free_vector(&vec1);
    free_vector(&vec2);
}

int main() {
    test_push_back();
    test_insert();
    test_replace();
    test_operations();
    return 0;
}
