# Labs

## Description
This repository contains labs of 2'd semester of NRNU MEPhI's program "Software Engineering".

## Project structure

- Every lab is located in own separated directories.
- Every directory contain:
  - Source code
  - Task description
  - Additional materials
  - Report

## List of labs

1. **Abstract data type**: Write an implementation of an abstract data type in C, a polymorphic collection
   based on a dynamic array. Write a wrapper program to test this
   implementation.
   - [Task](lab1/ЛР-1.pdf)
   - [Solution](lab1/lb1.md)
2. **Inherited abstract data types**: Write a system of inherited abstract data types in C++: Dynamic array, 
Linked List and Sequence. Sequence abstract data type must be abstraction providing unified access to another ADT.
    - [Task](lab2/ЛР-2.pdf)

## Installation

Clone repository with `git`
```bash
git clone https://gitlab.com/EkaMag/labs.git
```